import { Component } from '@angular/core';
import { Camera, CameraResultType } from '@capacitor/camera';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public imageUrl: any;

  constructor() {

    this.imageUrl = 'assets/shapes.svg';

  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 70,
      allowEditing: true,
      //resultType: CameraResultType.Uri, //for phone 
      resultType: CameraResultType.Base64, //for PWA
      correctOrientation: true
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)

    //this.imageUrl = image.webPath; //for android
    this.imageUrl = 'data:image/jpeg;base64,' + image.base64String; //for PWA
  }

}
