import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform } from '@ionic/angular';
import { Geolocation } from '@capacitor/geolocation';
import { Router } from '@angular/router';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  public coords: any;

  constructor(private platform: Platform, private loadingController: LoadingController, private router: Router, private newsService: NewsService) {

    this.coords = {
      latitude: null,
      longitude: null
    };

  }

  async ngOnInit() {

    await this.presentLoading();
    try {
      const response = await Geolocation.getCurrentPosition();
      console.log(response);
      this.coords = response.coords;

    }catch (err) {
      console.log(err);
    }
    await this.loadingController.dismiss();

  }

  async presentLoading(){

    const loading = await this.loadingController.create({
      message: 'Getting location...',
    });
    await loading.present();
  }

  isAndroid() {

    // if platform is android, return true
    return this.platform.is('android'); //return true or false

  }

  //sudah transfer ke custom-button.component.ts
  // pageMap() {

  //   this.router.navigateByUrl('/map');
  //   this.newsService.setCoords(this.coords);
  // }

}
