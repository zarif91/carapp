import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  public baseURL = 'https://newsapi.org/v2';
  public apiKey = 'a9c06036cbe24a40963c68a851235d91';
  public newsData: any;
  public coords: any;

  constructor(private httpClient: HttpClient) { }

  getNews(){
    const fullURL = this.baseURL + '/everything?q=tesla&from=2021-12-15&sortBy=publishedAt&apiKey=' + this.apiKey;
    return new Promise((resolve, reject) => {
        this.httpClient.get(fullURL)
        .subscribe((response: any) => {
            this.newsData = response.articles;
            resolve(response);
        }, (error) => { reject(error); });
    });
  }

  searchNewsById(id: any){
    const data = this.newsData.find((news: any) => news.publishedAt === id);
    if(data !== -1){
      return data;
    }
  }

  getBaseURL(){
    return this.baseURL;
  }

  setCoords(coords: any){

    this.coords = coords;
  }

  getCoords() {

    return this.coords || { latitude: 3.1321812, longitude: 101.4720605 };
  }


}
