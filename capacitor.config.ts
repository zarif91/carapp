import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.zarif.carapp',
  appName: 'CarApp',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
